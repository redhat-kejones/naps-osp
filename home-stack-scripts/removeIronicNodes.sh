#!/usr/bin/bash
source /home/stack/stackrc

for i in $(ironic node-list | grep -v UUID | awk '{print $2;}' | sed -e /^$/d); do ironic node-delete $i; done;
