#!/usr/bin/bash
source /home/stack/stackrc
cd ~

#--neutron-bridge-mappings datacentre:br-ex,tenant:br-tenant,storage:br-storage
#--neutron-network-vlan-ranges datacentre:1:1000,tenant:1:1000,storage:1:1000

openstack overcloud deploy --templates \
--control-flavor control \
--compute-flavor compute \
--ceph-storage-flavor ceph-storage \
--control-scale 1 \
--compute-scale 1 \
--ceph-storage-scale 1 \
--ntp-server ntp1.gatech.edu \
--neutron-tunnel-types vxlan \
--neutron-network-type vxlan \
-e /home/stack/templates/storage-environment.yaml \
-e /home/stack/templates/ceph-environment.yaml \
-e /home/stack/templates/network-environment-2nic.yaml \
-e /home/stack/templates/limits.yaml \
-e /home/stack/templates/firstboot-environment.yaml
