#!/usr/bin/bash

source /home/stack/stackrc

#Controllers
ironic node-update 69018cfa-d7f1-4a81-b550-4294217f9d73 add properties/capabilities='profile:control,boot_option:local'

#Computes
ironic node-update f5dbd4eb-e780-4c41-bfa7-669cb5216095 add properties/capabilities='profile:compute,boot_option:local'

#Cephs
ironic node-update 7773f9d8-8047-4fb4-ae50-188cdeee365f add properties/capabilities='profile:ceph-storage,boot_option:local'

