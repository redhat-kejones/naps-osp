# naps-osp

This is a collection of known working OSP-director heat templates, along with helper scripts and networking diagrams.

See Kevin Jones (kejones@redhat.com) or David Huff (dhuff@redhat.com) for access)

## Network Configs
* 2 nic configuration
  * network-environment-2nic.yaml
  * nic-configs-2nic
* 3 nic bonded configuration 
  * network-environment-3nic-bonded.yaml
* Advanced network isolation (4 nic, no bonding)
  * network-environment-adv.yaml
  * nic-config-adv

You should be able to either use these network configs straight away by doing a -e parameter to your overcloud deploy command. Or you can copy them to make modifications for differing environments.

-e /home/stack/templates/network-environment-2nic.yaml

or

-e /home/stack/templates/network-environment-3nic-bonded.yaml

or

-e /home/stack/templates/network-environment-adv.yaml

NOTE: if you are using cinder or swift nodes in your deployment, you may need to make additional changes to the templates. Also the nic-config files use the nic1, nic2, nic3 generic interface naming. You will most likely need to update this for customer deployments.
